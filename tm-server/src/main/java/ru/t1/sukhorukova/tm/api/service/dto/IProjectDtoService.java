package ru.t1.sukhorukova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final ProjectSort sort
    );

    @NotNull
    ProjectDTO create(
            @Nullable final String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    ProjectDTO updateById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    ProjectDTO updateByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    ProjectDTO changeProjectStatusById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull Status status
    );

    @NotNull
    ProjectDTO changeProjectStatusByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull Status status
    );

}
