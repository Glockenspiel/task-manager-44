package ru.t1.sukhorukova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.AbstractUserOwnerModelDTO;

import java.util.List;

public interface IUserOwnerDtoRepository<M extends AbstractUserOwnerModelDTO> extends IDtoRepository<M> {

    void add(@NotNull String userId, @NotNull M model);

    void update(@NotNull String userId, @NotNull M model);

    void remove(@NotNull String userId, @NotNull M model);

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    void removeOneById(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

    long getSize(@NotNull String userId);

}
