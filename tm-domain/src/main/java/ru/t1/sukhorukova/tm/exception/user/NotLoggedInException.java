package ru.t1.sukhorukova.tm.exception.user;

public final class NotLoggedInException extends AbstractUserException {

    public NotLoggedInException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}
